Chat
====

This is a simple tcp chat, written with golang.

To run server: ```go run server/server.go```

To run client: ```go run client/client.go```

To write direct message follow the format: ```@username text```

