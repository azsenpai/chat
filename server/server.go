package main

import (
	"bitbucket.org/azsenpai/chat/chat"
	"fmt"
	"net"
	"sync"
)

func main() {
	l, err := net.Listen("tcp", ":8080")

	if err != nil {
		fmt.Println("err: " + err.Error())
		return
	}

	defer l.Close()

	connMap := &sync.Map{}

	for {
		conn, err := l.Accept()

		if err != nil {
			fmt.Println("err: " + err.Error())
			continue
		}

		go handleUserConnection(conn, connMap)
	}
}

func handleUserConnection(conn net.Conn, connMap *sync.Map) {
	username := ""

	defer conn.Close()

	defer func() {
		if len(username) == 0 {
			return
		}

		connMap.Delete(username)
	}()

	for {
		reqMsg, err := chat.GetMessage(conn)

		if err != nil {
			fmt.Println("err: " + err.Error())
			break
		}

		fmt.Printf("from: %s, to: %s, body: %s\n", reqMsg.From, reqMsg.To, reqMsg.Body)

		if len(reqMsg.To) == 0 {
			chat.Broadcast(reqMsg, connMap)
		} else if reqMsg.To == chat.ServerUsername {
			if reqMsg.Body == chat.LoginAction && len(username) == 0 {
				username = reqMsg.From
			}

			chat.HandleUserAction(reqMsg, conn, connMap)
		} else {
			chat.DirectMessage(reqMsg, conn, connMap)
		}
	}
}
