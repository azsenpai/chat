package chat

import (
	"fmt"
	"io"
	"net"
	"strings"
)

const Base = 256
const MaxLen = Base * Base

type Message struct {
	From string
	To   string
	Body string
}

func (msg Message) String() string {
	return fmt.Sprintf("%s %s %s", msg.From, msg.To, msg.Body)
}

func (msg Message) Bytes() []byte {
	s := msg.String()
	l := len(s)

	bytes := make([]byte, 0)

	bytes = append(bytes, byte(l/Base))
	bytes = append(bytes, byte(l%Base))

	bytes = append(bytes, []byte(s)...)

	return bytes
}

func (msg Message) Send(conn net.Conn) {
	s := msg.String()

	if len(s) > MaxLen {
		remain := Message{
			From: msg.From,
			To:   msg.To,
		}

		k := MaxLen - len(fmt.Sprintf("%s %s ", msg.From, msg.To))

		if k < 0 {
			fmt.Println("unexpected situation")
			return
		}

		remain.Body = msg.Body[k:]
		msg.Body = msg.Body[:k]

		msg.Send(conn)
		remain.Send(conn)

		return
	}

	conn.Write(msg.Bytes())
}

func GetMessage(conn net.Conn) (msg Message, err error) {
	buf := make([]byte, 2)

	n, err := io.ReadFull(conn, buf)

	if err != nil {
		return msg, err
	}

	if n != 2 {
		return msg, fmt.Errorf("Can't read %d bytes", 2)
	}

	bufSize := int(buf[0])*Base + int(buf[1])
	buf = make([]byte, bufSize)

	n, err = io.ReadFull(conn, buf)

	if err != nil {
		return msg, err
	}

	if n != bufSize {
		return msg, fmt.Errorf("Can't read %d bytes", bufSize)
	}

	s := string(buf)
	p := strings.SplitN(s, " ", 3)

	if len(p) != 3 {
		return msg, fmt.Errorf("Undefined format")
	}

	msg.From = p[0]
	msg.To = p[1]
	msg.Body = p[2]

	return msg, nil
}

func DisplayMessage(msg Message) {
	format := "@%s says: %s"

	if len(msg.To) > 0 {
		format = "from @%s: %s"
	}

	fmt.Printf("\r"+format+"\n", msg.From, msg.Body)
}
