package chat

import (
	"errors"
	"fmt"
	"regexp"
	"strings"
)

func IsValidUsername(username string) (bool, error) {
	if len(username) < 1 || len(username) > 10 {
		return false, errors.New("username length must be: 0 < length <= 10")
	}

	re := regexp.MustCompile(`^[A-Za-z0-9]*$`)

	if !re.MatchString(username) {
		return false, errors.New("Only alphanumeric characters allowed")
	}

	if username == ServerUsername {
		return false, fmt.Errorf("username \"%s\" already taken", username)
	}

	return true, nil
}

func GetUsername(s string) (string, error) {
	s = strings.Trim(s, "\n\r ")

	_, err := IsValidUsername(s)

	if err != nil {
		return "", err
	}

	return s, nil
}

func GetUsernameWithBody(s string) (username string, body string) {
	s = strings.TrimLeft(s, " ")
	s = strings.TrimRight(s, "\n\r")

	if !strings.HasPrefix(s, "@") {
		username = ""
		body = s

		return
	}

	index := strings.Index(s, " ")

	if index < 0 {
		username = s[1:]
		body = ""

		return
	}

	username = s[1:index]

	if index+1 < len(s) {
		index++
	}

	if username == ServerUsername {
		username = ""
		index = 0
	}

	body = s[index:]

	return
}

func DisplayUsername(username string) {
	fmt.Printf("%s> ", username)
}
