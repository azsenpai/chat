package chat

import (
	"fmt"
	"net"
	"sync"
)

const ServerUsername = "server"
const LoginAction = "login"

func Broadcast(msg Message, connMap *sync.Map) {
	connMap.Range(func(key, value interface{}) bool {
		if username, ok := key.(string); ok && username == msg.From {
			return true
		}

		if conn, ok := value.(net.Conn); ok {
			msg.Send(conn)
		}

		return true
	})
}

func HandleUserAction(msg Message, conn net.Conn, connMap *sync.Map) {
	respMsg := Message{
		From: ServerUsername,
		To:   msg.From,
	}

	switch msg.Body {
	case LoginAction:
		_, ok := connMap.Load(msg.From)

		if ok {
			respMsg.Body = fmt.Sprintf("username \"%s\" already taken", msg.From)
		} else {
			connMap.Store(msg.From, conn)
			respMsg.Body = "ok"
		}

	default:
		respMsg.Body = "unknown action"
	}

	respMsg.Send(conn)
}

func DirectMessage(msg Message, conn net.Conn, connMap *sync.Map) {
	respMsg := msg
	respConn := conn

	value, ok := connMap.Load(msg.To)

	if !ok {
		respMsg.From = ServerUsername
		respMsg.To = msg.From
		respMsg.Body = fmt.Sprintf("user @%s not found", msg.To)
	} else if conn, ok := value.(net.Conn); ok {
		respConn = conn
	} else {
		respConn = nil
	}

	if respConn != nil {
		respMsg.Send(respConn)
	}
}
