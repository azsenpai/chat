package main

import (
	"bitbucket.org/azsenpai/chat/chat"
	"bufio"
	"fmt"
	"net"
	"os"
)

func main() {
	conn, err := net.Dial("tcp", "localhost:8080")

	if err != nil {
		fmt.Println("err: " + err.Error())
		return
	}

	defer conn.Close()

	username, err := login(conn)

	if err != nil {
		fmt.Println("err: " + err.Error())
		return
	}

	go sendToServer(username, conn)
	listenServer(username, conn)
}

func login(conn net.Conn) (string, error) {
	for {
		fmt.Printf("username: ")
		reader := bufio.NewReader(os.Stdin)

		line, err := reader.ReadString('\n')

		if err != nil {
			return "", err
		}

		username, err := chat.GetUsername(line)

		if err != nil {
			fmt.Println("err: " + err.Error())
			continue
		}

		reqMsg := chat.Message{
			From: username,
			To:   chat.ServerUsername,
			Body: chat.LoginAction,
		}

		reqMsg.Send(conn)

		respMsg, err := chat.GetMessage(conn)

		if err != nil {
			return "", err
		}

		if respMsg.Body == "ok" {
			return username, nil
		}

		chat.DisplayMessage(respMsg)
	}
}

func sendToServer(username string, conn net.Conn) {
	for {
		chat.DisplayUsername(username)
		reader := bufio.NewReader(os.Stdin)

		line, err := reader.ReadString('\n')

		if err != nil {
			fmt.Println("err: " + err.Error())
			return
		}

		to, body := chat.GetUsernameWithBody(line)

		reqMsg := chat.Message{
			From: username,
			To:   to,
			Body: body,
		}

		reqMsg.Send(conn)
	}
}

func listenServer(username string, conn net.Conn) {
	for {
		respMsg, err := chat.GetMessage(conn)

		if err != nil {
			fmt.Println("err: " + err.Error())
			return
		}

		chat.DisplayMessage(respMsg)
		chat.DisplayUsername(username)
	}
}
